from argparse import ArgumentParser, FileType
import logging
import json

import requests

from producer import delivery_callback, Producer


LIMIT_REQUEST = 100
OFFSET_REQUEST = 100
TOPIC = "properties"
KEY = "fincaraiz"


url = "https://api.fincaraiz.com.co/document/api/1.0/listing/search"

with open("payload.json", "r") as f:
    payload = json.loads(f.read())

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'Referer': 'https://www.fincaraiz.com.co/',
    'Content-Type': 'application/json',
    'Origin': 'https://www.fincaraiz.com.co'
}


def obtain_properties(producer):
    for offset in range(0, 1000, OFFSET_REQUEST):
        payload["offset"] = offset
        payload["limit"] = LIMIT_REQUEST
        response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
        logging.info(f"Offset {offset}, {response.status_code}")
        producer.produce(TOPIC, response.text, KEY, callback=delivery_callback)
    producer.poll(10000)
    producer.flush()


def main():
    parser = ArgumentParser()
    parser.add_argument("config_file", type=FileType("r"))
    args = parser.parse_args()
    producer = Producer(args)
    obtain_properties(producer.producer)


if __name__ == "__main__":
    logging.basicConfig(filename="responses.log",
                        level=logging.INFO,
                        format='%(asctime)s %(message)s')
    main()
