from configparser import ConfigParser
import logging

import confluent_kafka


class Producer:

    def __init__(self, args):
        config_parser = ConfigParser()
        config_parser.read_file(args.config_file)
        config = dict(config_parser["default"])
        self.producer = confluent_kafka.Producer(config)


def delivery_callback(err, msg):
    if err:
        logging.error('ERROR: Message failed delivery: {}'.format(err))
    else:
        logging.info("Produced event to topic {topic}: key = {key:12} value = {value:12}".format(
            topic=msg.topic(), key=msg.key().decode('utf-8'), value=msg.value().decode('utf-8')))
